#!/usr/bin/env python
# -*- coding:utf-8 -*-

import urllib

from bs4 import BeautifulSoup as _beautifulsoup

try:
    __import__('lxml')
    BeautifulSoup = lambda markup, from_encoding=None: _beautifulsoup(markup, 'lxml', from_encoding=from_encoding)
except ImportError:
    try:
        __import__('html5lib')
        BeautifulSoup = lambda markup, from_encoding=None: _beautifulsoup(markup, 'html5lib', from_encoding=from_encoding)
    except ImportError:
        BeautifulSoup = lambda markup, from_encoding=None: _beautifulsoup(markup, 'html.parser', from_encoding=from_encoding)


def quote(string, encoding='utf-8'):
    if isinstance(string, unicode):
        string = string.encode(encoding)
    return urllib.quote(string)


def unquote(string, encoding='utf-8'):
    return urllib.unquote(string).decode(encoding)


def to_str(obj):
    """
    convert a object to string
    """
    if isinstance(obj, str):
        return obj
    if isinstance(obj, unicode):
        return obj.encode('utf-8')
    return str(obj)


def to_unicode(obj):
    """
    convert a object to unicode string
    """
    return obj.decode('utf-8')
