#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re
from distutils.core import setup

with open('scpy/__init__.py', 'r') as fp:
    VERSION = re.findall(ur"__version__.+?['\"](.+)['\"]", fp.read())[0]

setup(
    name='scpy',
    version=VERSION,
    description='SocialCredits python lib',
    url='https://git.oschina.net/socialcredits/social-credits-py',
    author='SocialCredits',
    author_email='kai.yan@socialcredits.cn',
    license='MIT',
    # package_dir={'scpy': ''},
    packages=['scpy'],
)
