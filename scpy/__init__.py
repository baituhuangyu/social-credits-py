#!/usr/bin/env python
# -*- coding:utf-8 -*-

import date_extractor
import logger
import request_util
import xawesome_codechecker
import xawesome_location
import xawesome_mail
import xawesome_time
import xawesome_util


__version__ = '1.1.10'


_URL = 'https://git.oschina.net/socialcredits/social-credits-py/raw/master/scpy/__init__.py'
_VERSION_PATTERN = ur"__version__.+?['\"](.+)['\"]"
_WARNING_TEXT = '\n\n发现新版本[%s]， 当前版本[%s]。\n\n请使用[pip install git+https://git.oschina.net/socialcredits/social-credits-py --upgrade]安装新版本。\n\n'

try:
    import urllib2 as _urllib2, re as _re
    _new_version = _re.findall(_VERSION_PATTERN, _urllib2.urlopen(_URL, timeout=0.1).read())[0]
    # _new_version = re.findall(_VERSION_PATTERN, requests.get(_URL, timeout=1).content)[0]
    if _new_version != __version__:
        import warnings as _warnings
        _warnings.warn(_WARNING_TEXT % (_new_version, __version__))
except Exception, e:
    pass
