# -*- coding:utf-8 -*- 

import logging
import logging.config

import sc_logging_conf as CONFIG


default_path='sc_logging_conf.json', 
default_level=logging.INFO,

logging.config.dictConfig(CONFIG.SC_LOGGING_CONF)

logger = logging

def get_logger(filename):
    return logging.getLogger(filename)

if __name__ == '__main__':
    print "main"
    logging.warning("main test")
    scLogger = getScLogger("logging test")
    scLogger.info("test")
