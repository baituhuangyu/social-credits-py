#social-credits-py


---

### date_extractor：从字符串中取时间

所有方法都返回datetime.datetime 对象，如果没找到，返回None,下面是例子

```
from scpy.date_extractor import extract_chinese_date,format_date_str,extract_first_date
```
>中文日期
```python
test_1 = u'我是测试字符串网二〇一五年七月十五日中国共产'
print extract_chinese_date(test_1)
```

>从字符串中取各种时间
```python
test_2 = u'2014年12月23日法范德萨'
print extract_first_date(test_2)
```

>主要格式化数字不是两位和时间缺失或部分缺失,分隔符需要是'-' 和'：'如果不是请使用test_2
```python
test_3 = '2015-9-7'
print format_date_str(test_3)
```

--- 

###url请求封装

 - RequestUtil 一个全局的requestUtil ，默认会使用同一个request.session来发送请求

```Python

from scpy.request_util import RequestUtil

proxy_config = {
        'ip':u'代理库存放ip',
        'port':27017,   
        'db':u'代理库名',
        'collection':u'代理collection',
    }

request_util = RequestUtil(proxy_config) #proxy_config 缺失，则不使用代理
request_util.make_reqeust(url)    #默认为get请求默认isproxy=False 不使用代理
```
   

---

### xawesome_time: 时间格式化

#####功能： 格式化时间字符串为标准格式
 - 标准格式：
     > "现在是2012-10-02 02:01:01"将返回`2012-10-02 02:01:01`


 - 普通格式：
     > "现在是2012-10-02"将返回`2012-10-02 00:00:00`
   "现在是2012年10月02日"将返回`2012-10-02 00:00:00`
   "现在是2012年10月02日13时27分4秒"将返回`2012-10-02 03:27:04`
   ......

 - 中文格式（仅支持到日，时分秒中文格式暂不支持，由于实际意义不大，可预见的将来也不会支持）：
     > "现在是二〇一五年十二月二十五日"将返回`2015-12-25 00:00:00`

 - 时间表达式：
     > "3天后" 将返回距离调用时间三天之后的时间的字符串表示
   同理，可支持： "2年前", "1个季度之后", "5天以前", "3小时后", ...
      

#####用法：

```Python
from scpy import xawsome_time

xawsome_time.parse_time(TIME_STR)
```

 - 参数： 
     > time_str: [str, unicode] 时间字符串
   convert_2_time_obj: [bool] True => 转换为对象 | False => 转换为字符串 | default value: False


---

### xawesome_mail: 发送邮件

#####功能： 发送提醒邮件邮件

 **注意**: 应用场景为程序运行过程中的提醒邮件，不可用于大量发送邮件的场景。


#####用法：

```Python
from scpy import xawsome_mail

xawsome_mail.send_mail('中文主题', u'中文正文', ['xu.du@socialcredits.cn'], sender_nickname='发信人')
```

 - 参数： 
```
    subject    : 邮件主题
    content    : 邮件正文
    mailto_list: 邮件接收者列表
    file_path  : 附件路径（没有则为空）
    kwargs: 关键字参数（ATT: 后面三个参数必须同时制定或者同时不指定）：
                sender_nickname ： 发件人昵称
                sender_mail_addr： 定制发件人邮件地址
                sender_mail_pass： 定制发件人邮箱密码
                sender_mail_host： 定制发件人邮箱服务器
```     
     
---

### xawesome_location: 位置相关服务

#####用法：

```Python
from scpy.xawesome_location import parse_location

parse_location(u'杭州誉存科技有限公司')

>>> {"province": "浙江", "cityShortName": "杭州", "cityName": "杭州市", "id": "3301"}
```

 - 参数： 
    company_name : 公司全称
 - 返回：
    {"id": "地区编号", "cityName": u"城市全称", "province": u"省份", "cityShortName": "城市简称"}


---

### xawesome_codechecker

#### 测试函数运行时间 @timeit 

```Python
from scpy.xawesome_codechecker import timeit
@timeit
def func():
    pass

>>>func()
function [func] start at [2015-11-03 15:04:49]
function [func] exit  at [2015-11-03 15:04:49]
function [func] coast [0.0350475311279ms]
```

#### 控制函数不抛出异常 @no_exception
 - 有时候不希望代码在某一句停下，但每句代码 `try-catch` 又很难看，可以使用 `@no_exception` 控制函数在遇到异常时返回某默认值。

#### 获取程序运行的主机信息
 - 程序可能运行在开发机/测试机/线上或其他环境，在发警告邮件时不容易区分来自哪里，可使用`get_user()`获取当前运行程序的用户名， `get_ip()`获取本机IP， `get_runner()`获取类似`user@host`格式的信息。

---

### xawesome_util

 - 建议通过这里导入`BeautifulSoup`，会自动检测并使用系统已安装的html parser。
 - url 编码解码工具。


     